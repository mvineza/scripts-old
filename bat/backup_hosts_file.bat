SET timestamp=%date:~4,2%-%date:~7,2%-%date:~-4%-%time:~0,2%-%time:~3,2%-%time:~6,2%
SET backup_folder=K:\Others\config\
SET target_file=%systemroot%\system32\drivers\etc\hosts
SET backup_file=%backup_folder%hosts.%timestamp%

call "C:\Users\merrell\Documents\Batch Scripts\unlock_backup_drive.bat"
copy %target_file% %backup_file%
call "C:\Users\merrell\Documents\Batch Scripts\lock_backup_drive.bat"