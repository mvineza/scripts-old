#!/usr/bin/env python3
"""
This will setup the correct symlinks to `/usr/local/bin` for *nix
platforms and copy the appropriate scripts to correct folder for
windows hosts.
"""

__author__ = "Merrell Vineza"
__date__ = "09/03/2017"
__version__ = "1.0.0"
__email__ = "rell.vineza14@yahoo.com"

import sys
import os

bash_dir = 'bash'
python_dir = 'python'
profile_dir = 'profiles'
config_dir = 'configs'

def create_symlinks(dir, symlink_dir):

  try:
    os.mkdir(symlink_dir)
    # TODO: find a better replacement on these raw shell commands
    #       since `os.mkdir` is not applying correct permissions
    #       when using `mode=755`
    os.system('chmod 755 '+symlink_dir)
    os.system('chown root:root '+symlink_dir)
  except FileExistsError:
    pass

  files = os.listdir(dir)
  os.chdir(dir)
  for f in files:
    source_file = os.path.abspath(f)
    symlink = symlink_dir + f
    try:
      os.remove(symlink)
    except FileNotFoundError:
      pass
    os.symlink(source_file, symlink)
    print('{} -> {}'.format(symlink, source_file))
  os.chdir('..')

def setup_py_scripts():

  # TODO: obviously, put a code here :)
  pass

if sys.platform == 'linux':
  if os.getuid() != 0:
    print('Run this script as root')
    sys.exit(1)
  else:
    print('links created:')
    create_symlinks(bash_dir, '/usr/local/bin/')
    create_symlinks(python_dir, '/usr/local/bin/')
    create_symlinks(profile_dir, '/home/merrell/')
    create_symlinks(config_dir, '/home/merrell/')
elif sys.platform == 'win32':
  setup_py_scripts()
