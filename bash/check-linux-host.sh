#!/bin/bash

for host in `cat /tmp/server.lst`; do
  echo "*** checking $host ***"
  nmap -p 22 $host | grep ssh | grep open
  if [ $? -eq 0 ]; then
    echo $host >> /tmp/linux_ssh_open.lst
  fi
done
