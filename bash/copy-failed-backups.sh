#!/bin/bash
#
# Description:
#   This copies agaion all myfedora backups that were not transferred
#   to merrellpc due to any sort of issue (network, disk full, etc)
#
# Revisions:
#   mvineza - MM/DD/YYYY HH:SS - base code
#   mvineza - 08/20/2017 19:56 - added checks for remote host
#
# Author:
#   Merrell Vineza (rell.vineza14@yahoo.com)
#

FAILED_LIST="/home/merrell/Documents/notes/backup_myfedora.failed_copies"
TARGET_LOCATION="/mnt/merrell/Tmp"
REMOTE_HOST="merrellpc"

quit () {
  [ -n "$1" ] \
    && printf "error: %s\n" "$1"
  exit 1
}

[ $(id -u) != "0" ] \
  && quit "run script as root"

echo "Contacting $REMOTE_HOST .."
ping -qc 1 $REMOTE_HOST &> /dev/null \
  || { echo "unable to reach $REMOTE_HOST"; exit 1; }

echo "copying files .."
for file in $(cat $FAILED_LIST); do
  cp -vp $file $TARGET_LOCATION/
done

echo > $FAILED_LIST
chown merrell:merrell $FAILED_LIST

exit 0
