#!/bin/bash
#
# ================================================================ #
# Program: backup-to-bitlocker.sh
#
# Description:
#   Mounts a bitlocker drive and run python backup script.
#   drive will be unmounted afterwards
#
# Author: Merrell Vineza (rell.vineza14@yahoo.com)
# Date: 10/01/2017 08:49:00 PHT
# ================================================================ #
#

# ================================================================ #
# DATA
# ================================================================ #

BITLOCKER_DRIVE="$1"
BITLOCKER_RECOVERY_KEY="/home/merrell/.bitlocker_recovery_key"
BACKUP_TYPE="$2"
BACKUP_DESTINATION="$3"

# ================================================================ #
# FUNCTIONS
# ================================================================ #

usage () {
cat << EOF
Usage:
  $0: BITLOCKER_DRIVE BACKUP_TYPE BACKUP_LOCATION

Where:
  BITLOCKER_DRIVE - in the form of "/dev/sdXX"
  BACKUP_TYPE - "full" or "inc" 
  BACKUP_DESTINATION - full path to where your backups will go
EOF
  exit 0
}

quit () {
  [ -n "$1" ] \
    && printf "error: %s\n" "$1"
  exit 1
}

unmount_bitlocker_drive () {
  MNT_POINT="$(df | grep ${BITLOCKER_DRIVE/\/dev\//}_bitlocker | awk '{print $6}')"
  umount $MNT_POINT
}

msg () {
  [ -n "$1" ] \
    && printf "********* %s *********\n" "$1"
}

# ================================================================ #
# MAIN
# ================================================================ #

[[ -z $1 && -z $2 && -z $3 ]] \
  && usage

[[ "$(id -u)" != "0" ]] \
  && quit "run this script as root"

msg "mounting bitlocker drive .."
mount-bitlocker-drive.sh $BITLOCKER_DRIVE $BITLOCKER_RECOVERY_KEY
mkdir -p $BACKUP_DESTINATION
echo
msg "backing up files .."
backup-my-files.py $BACKUP_TYPE $BACKUP_DESTINATION
echo
msg "unmounting bitlocker drive .."
unmount_bitlocker_drive
echo
