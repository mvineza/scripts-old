#!/bin/bash
#
# Filename: happy_birthday.sh
# Description: A simple presentation for my girlfriend's birthday.
# Author: Merrell V. Vineza
# Date: 10/01/2018
#

msg() {
  echo $1 | pv -qL 20
}

pause () {
  echo .
  sleep 1
  echo .
  sleep 1
  echo .
  sleep 1
}

msg "Hi Love! I made a simple presentation for you."
msg "I hope you'll like it."
pause
msg "Gel: Will you sign up for the journey with me?"
pause
msg "Rel: Can I apply for a lifetime membership?"
pause
msg "Gel: No return. No exchange. No turning back."
pause
msg "Rel: Take all my money!"
pause
figlet "Jump on the next chapter with me.."
pause
figlet "I'll love you till the end.."
pause
figlet "Happy Birthday my Love!"
