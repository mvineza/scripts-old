#!/bin/bash
#
# ================================================================ #
# Program: stash-vault-files.sh
#
# Description:
#   Can be used to quickly stash vauilt file by moving it to ~/Tmp
#   so you can merge any branch to your branch.
#
# Author: Merrell Vineza (rell.vineza14@yahoo.com)
# Date: 09/28/2017 08:59:00 PHT
# ================================================================ #
#

# ================================================================ #
# DATA
# ================================================================ #
ENVS="development
      testing
      integration
      slt
      staging
      production"
CURRENT_BRANCH="$(git branch | awk '/*/ {print $2}')"
TIMESTAMP="$(date +'%m-%d-%Y-%H-%M-%S')"
VAULT_FILE_BASE_DIR="playbooks/kana/group_vars"

# ================================================================ #
# FUNCTIONS
# ================================================================ #

# ================================================================ #
# MAIN
# ================================================================ #


for i in $ENVS; do
  SOURCE_VAULT_DIR="$VAULT_FILE_BASE_DIR/$i"
  DESTINATION_VAULT_DIR="/home/merrell/Tmp/${CURRENT_BRANCH}-${TIMESTAMP}/${i}"
  mkdir -p $DESTINATION_VAULT_DIR
  mv $SOURCE_VAULT_DIR/vault.yml $DESTINATION_VAULT_DIR
done

gc "stashing vault files - $TIMESTAMP"

echo ""
echo "vault files removed, you may now merge any branch you want."
