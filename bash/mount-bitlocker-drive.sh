#!/bin/bash
#
# ================================================================ #
# Program: mount_bitlocker_drive.sh
#
# Description:
#   Mounts any bitlocker-encrypted drive
#
# Author: Merrell Vineza (rell.vineza14@yahoo.com)
# Date: 09/30/2017 19:25:00 PHT
# ================================================================ #
#

# ================================================================ #
# DATA
# ================================================================ #

PROGNAME="${0##*/}"
BITLOCKER_PARTITION="$1"
PARTITION_NAME="${BITLOCKER_PARTITION/\/dev\//}"
BITLOCKER_FILE_PATH="/tmp/dislocker-${PARTITION_NAME}"
BITLOCKER_FILE="$BITLOCKER_FILE_PATH/dislocker-file"
BITLOCKER_MNT_POINT="/mnt/${PARTITION_NAME}_bitlocker"
PARTITION_LIST="/tmp/$PROGNAME.paritions"
RECOVERY_KEY_FILE="$2"
SIZE_LIST="/tmp/$PROGNAME.sizes"

# ================================================================ #
# FUNCTIONS
# ================================================================ #

usage() {

cat << EOF
Usage: $PROGNAME BITLOCKER_PARTITION [RECOVERY_KEY_FILE]

  If you provide the path to the recovery key, that will
  be used instead of user-supplied password.

Here are list of NTFS bitlocker drive detected:

$(generate_partition_list 2> /dev/null)

For more information on the drives, use "sudo fdisk -l | grep sd"
EOF

  cleanup
  exit 0
}

quit() {
  [ -n "$1" ] \
    && printf "error: %s\n" "$1"
  exit 1
}

generate_partition_list() {
  fdisk -l \
   | egrep ^/dev/sd \
   | grep NTFS \
   | awk '{print $1}' > $PARTITION_LIST
  fdisk -l \
   | egrep ^/dev/sd \
   | grep NTFS \
   | rev \
   | awk '{print $3}' \
   | rev > $SIZE_LIST
  paste $PARTITION_LIST \
        $SIZE_LIST \
        | awk '{print $1,"=",$2}'
}

cleanup() {
  rm -f $PARTITION_LIST $SIZE_LIST
}

# ================================================================ #
# MAIN
# ================================================================ #

[[ "$(id -u)" != "0" ]] \
  && quit "run this script as root"

[[ -z "$BITLOCKER_PARTITION" ]] \
  && usage

mkdir -p $BITLOCKER_FILE_PATH
mkdir -p $BITLOCKER_MNT_POINT

echo "Generating $BITLOCKER_FILE .."
if [[ -z "$RECOVERY_KEY_FILE" ]]; then
  dislocker -V $BITLOCKER_PARTITION -u -- $BITLOCKER_FILE_PATH
elif [[ -n "$RECOVERY_KEY_FILE" ]]; then
  [ ! -f "$RECOVERY_KEY_FILE" ] \
    && quit "recovery key file missing"
  RECOVERY_KEY="$(cat $RECOVERY_KEY_FILE)"
  dislocker -V $BITLOCKER_PARTITION --recovery-password=$RECOVERY_KEY -- $BITLOCKER_FILE_PATH
fi
echo "Mounting $BITLOCKER_FILE to $BITLOCKER_MNT_POINT .."
mount $BITLOCKER_FILE $BITLOCKER_MNT_POINT

cleanup
