#!/bin/bash
#
# ================================================================ #
# Program: rdp.sh
#
# Description:
#   Connects to bayviewer server using xfreefdp command.
#
# Author: Merrell Vineza (rell.vineza14@yahoo.com)
# Date: 02/10/2018 12:12:39 PHT
# ================================================================ #
#

# ================================================================ #
# DATA
# ================================================================ #
SCREEN_SIZE="$1"

# ================================================================ #
# FUNCTIONS
# ================================================================ #

help() {
 cat << EOF
Usage: ${0} SCREEN_SIZE
Where:
  SCREEN_SIZE is in percentage (95%) or dimensions (1090x1080)
EOF
  exit 0
}

# ================================================================ #
# MAIN
# ================================================================ #

[ -z "$SCREEN_SIZE" ] && help

xfreerdp /v:luzon.esl-asia.com /u:esl-asia\\mvineza \
  /gt:rpc \
  /load-balance-info:"tsv://MS Terminal Services Plugin.1.All_Users" \
  /cert-ignore \
  /cert-tofu \
  /gdi:sw \
  /g:gate.esl-asia.com \
  /size:$SCREEN_SIZE \
  +clipboard
