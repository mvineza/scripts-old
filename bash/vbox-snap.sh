#!/bin/bash
#
# ================================================================ #
# Program: vbox-snap.sh
#
# Description:
#   Manages virtualbox snapshots
#
# Author: Merrell Vineza (rell.vineza14@yahoo.com)
# Date: 06/10/2018 11:21:04 PHT
# ================================================================ #
#

# ================================================================ #
# DATA
# ================================================================ #

# ================================================================ #
# FUNCTIONS
# ================================================================ #

help () {
cat << EOF
Usage:
  $0 <-c|-d|-r> SNAPSHOT_INFO_FILE

Where:
  SNAPSHOT_INFO_FILE has the following format:
    <snapshot name>
    <vmname 1>
    <vmname 2>
    ...
  -c -- create
  -d -- delete
  -r -- restore
EOF
  exit 0
} 

snap () {
  SNAPSHOT_INFO=$OPTARG
  if [ ! -f $SNAPSHOT_INFO ]; then
    echo "$SNAPSHOT_INFO doesn't exist"
    exit 1
  else
    SNAPSHOT_NAME=$(cat $SNAPSHOT_INFO | head -1)
    VM_LIST=$(cat $SNAPSHOT_INFO | grep -v $SNAPSHOT_NAME)
    for host in $VM_LIST; do
      if [[ $1 == "take" ]]; then
        echo "Taking online $SNAPSHOT_NAME of $host .."
        vboxmanage snapshot $host $1 $SNAPSHOT_NAME
	echo
      elif [[ $1 == "delete" ]]; then
        echo "Deleting $SNAPSHOT_NAME of $host .."
        vboxmanage snapshot $host $1 $SNAPSHOT_NAME
	echo
      elif [[ $1 == "restore" ]]; then
        echo "Restoring $host to $SNAPSHOT_NAME .."
        vboxmanage controlvm $host poweroff
	sleep 1
        vboxmanage snapshot $host $1 $SNAPSHOT_NAME
	sleep 1
        vboxmanage startvm $host
	echo
      fi
    done
  fi
  exit 0
}

# ================================================================ #
# MAIN
# ================================================================ #

[[ -z $* ]] && help

while getopts ":c:d:r:" opt; do
  case $opt in
    c) snap take ;;
    d) snap delete ;;
    r) snap restore ;;
    *) help ;;
  esac
done
