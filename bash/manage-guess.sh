#!/bin/bash
#
# ================================================================ #
# Program: put_name_here
#
# Description:
#   Enables/Disables SSH guess login that can be used to quickly
#   transfer fies.
#
# Author: Merrell Vineza (rell.vineza14@yahoo.com)
# Date: 09/22/2017 10:16:00 PHT
# ================================================================ #
#

# ================================================================ #
# DATA
# ================================================================ #
COMMAND="$1"

# ================================================================ #
# FUNCTIONS
# ================================================================ #

disable_guess() {
  usermod -s /bin/false guess
  systemctl stop sshd
}

enable_guess() {
  usermod -s /bin/bash guess
  systemctl start sshd
}

# ================================================================ #
# MAIN
# ================================================================ #

if [[ "$COMMAND" = "start" ]]; then
  enable_guess
elif [[ "$COMMAND" = "stop" ]]; then
  disable_guess
fi
