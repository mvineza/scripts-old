#!/bin/bash
#
# ================================================================ #
# Program: push-to-git-zim.sh
#
# Description:
#   Wrapper script to be used as Zim custom tools. Traditional
#   commands doesn't work quite well (e.g delete files are not
#   included in commits when `gc` is used) so we need to make a
#   wrapper script like this.
#
# Author: Merrell Vineza (rell.vineza14@yahoo.com)
# Date: 03/11/2018 11:02:26 AM PHT
# ================================================================ #
#

# ================================================================ #
# DATA
# ================================================================ #
GIT_PATH="/home/merrell/GitRepo/zim-notes"
COMMIT_MSG="$@"

# ================================================================ #
# FUNCTIONS
# ================================================================ #

# ================================================================ #
# MAIN
# ================================================================ #
cd $GIT_PATH || exit 1
/usr/local/bin/gc -f "$COMMIT_MSG" || exit 1
exit 0
