#!/bin/bash
#
# Description:
#   `Restarts VirtualBox NAT network`
#
# Revisions:
#   mvineza - 07/20/2017 10:19 - base code
#
# Author:
#   Merrell Vineza (rell.vineza14@yahoo.com)
#

vboxmanage natnetwork stop --netname nat
vboxmanage natnetwork start --netname nat
