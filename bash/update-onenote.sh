#!/bin/bash
#
# Simple bash script to update local Onenote files
# by deleting all locally then copying from remote
# source.
#

local_onenote="/home/merrell/Documents/OneNote Notebooks"
remote_onenote="/mnt/merrell/Documents/OneNote Notebooks"
mount_point="/mnt/merrell"
remote_host="merrellpc"

[ $(id -u) -ne "0" ] \
  && { echo "run script as root"; exit 1; }

echo "local OneNote path: $local_onenote"
echo "remote OneNote path: $remote_onenote"
echo ""
echo "Contacting $remote_host .."
ping -qc 1 $remote_host &> /dev/null \
  || { echo "unable to reach $remote_host"; exit 1; }

echo "Deleting local OneNote files .."
rm -fr "$local_onenote"
echo "Copying remote OneNote files .."
cp -Rp "$remote_onenote" "$(dirname "$local_onenote")"
chown -R merrell:merrell "$local_onenote"
umount $mount_point

exit 0
