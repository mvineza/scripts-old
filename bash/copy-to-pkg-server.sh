#!/bin/bash

FILE_LIST="/tmp/files.lst"
#SRC_HOST="mdc-ts-kn-app02.msgreen.dom" # TST2
SRC_HOST="mdc-ts-kn-app03.msgreen.dom" # TST3
#SRC_HOST="mdc-de-kn-app05.msgreen.dom" # TST4
PKG_SERVER="mdc-kn-pkg01.msgreen.dom"
DOWNLOAD_DIR="/home/merrell/Downloads"

for file in `cat $FILE_LIST`; do
  echo "Copying $file to localhost .."
  scp -r root@$SRC_HOST:$BUNDLE_DIR$file $DOWNLOAD_DIR
  echo

  echo "Copying $file to $PKG_SERVER .."
  scp -r $DOWNLOAD_DIR/$(basename $file) root@$PKG_SERVER:$file
  echo

  echo "Changing ownership to kanauser .."
  ssh root@$PKG_SERVER "chown -R kanauser:kanauser $file"
done

exit 0
