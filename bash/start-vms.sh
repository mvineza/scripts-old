#!/bin/bash

VMS="server client"

for vm in $VMS; do
  virsh start $vm
done

# start also virt-manager
echo "Starting virt-manager console.."
virt-manager

exit 0
