#!/bin/bash
#
# Simple shell script to open multiple tabs mate-terminal
# with each tab executing an initial command
#
# Merrell Vineza - rell.vineza14@yahoo.com - 07/07/2017 21:30 SGT
#
# REVISIONS:
#  merrell - 07/08/2017 12:54 - added check_input_file
#  merrell - 07/14/2017 08:00 - changed functionality
#
# TODO:
#  - launch tabs as login shell
#

SERVER_LIST="/home/merrell/Documents/notes/server.lst"
TARGET_COMMAND="$1"
TERM_ENTRY="$TARGET_COMMAND; bash"

quit () {
  [ -n "$1" ] \
    && printf "error: %s\n" "$1"
  exit 1 
}

list_servers () {
  for entry in $(cat $SERVER_LIST); do
    echo $entry
  done
}

usage () {
cat << EOF
Usage: multi_tab.sh [COMMAND]

Description:
  Program will execute the given command against
  servers in the list located in $SERVER_LIST

Current Servers
===============

$(list_servers)
EOF
exit 0
}

[ -z "$TARGET_COMMAND" ] \
  && usage

for server in $(cat $SERVER_LIST); do
  mate-terminal --tab --title "$server" --command "ssh mvineza@$server $TERM_ENTRY"
done

exit 0
