#!/bin/bash
#
# ================================================================ #
# Program: rdp-bayviewer.sh
#
# Description:
#   Connects to bayviewer host using `xfreerdp`.
#
# Author: Merrell Vineza (rell.vineza14@yahoo.com)
# Date: 10/28/2017 20:47:57
# ================================================================ #
#

# ================================================================ #
# DATA
# ================================================================ #

RES="$1"
TARGET_HOST="luzon.esl-asia.com"
GATEWAY_HOST="gate.esl-asia.com"
LOGIN_NAME="esl-asia\\mvineza"
LOAD_BALANCE_INFO="tsv://MS Terminal Services Plugin.1.All_Users"

# ================================================================ #
# FUNCTIONS
# ================================================================ #

quit () {
  [[ -n $1 ]] && \
    printf "error: %s\n" "$1"
  exit 1
}

# ================================================================ #
# MAIN
# ================================================================ #

[[ -z $1 ]] && quit "resolution required (e.g 90%, 1024x1080)"

/usr/bin/xfreerdp \
  /v:${TARGET_HOST} \
  /g:${GATEWAY_HOST} \
  /u:${LOGIN_NAME} \
  /size:${RES} \
  /load-balance-info:"${LOAD_BALANCE_INFO}" \
  /gt:rpc \
  /cert-ignore \
  /cert-tofu \
  /gdi:sw \
  /clipboard
