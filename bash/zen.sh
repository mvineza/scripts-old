#!/bin/bash
#
# ================================================================ #
# Program: zen.sh
#
# Description:
#   Wrapper script to bash commands that provides pop up message
#   on errors.
#
# Author: Merrell Vineza (rell.vineza14@yahoo.com)
# Date: 11/05/2017 14:10:00
# ================================================================ #
#

# ================================================================ #
# DATA
# ================================================================ #

CMD="$@"
ZENITY_USER="merrell"

# ================================================================ #
# FUNCTIONS
# ================================================================ #

quit () {
  zenity --error --text "$1" --display=:0.0
  exit 1
}

usage () {
  echo "Usage: ${0} \"COMMAND\""
}

# ================================================================ #
# MAIN
# ================================================================ #

[ "$#" -eq "0" ] && { usage; exit 1; }
$CMD || quit "error executing: $CMD"
