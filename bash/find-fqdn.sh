#!/bin/bash
#
# Description:
#   Loops through a series of domain names to find the correct
#   FQDN of the given host
#
# Revisions:
#   mvineza - 08/01/2017 12:47 - base code
#   mvineza - 08/07/2017 13:43 - added IP information in the output
#
# Author:
#   Merrell Vineza (rell.vineza14@yahoo.com)
#

DOMAINS="msgreen.dom
         msorange.dom
         msred.dom
         ph.esl-asia.com"
HOSTNAME_SHORT="$1"
HOSTNAME_FQDN=""

msg () {
  [ -n "$1" ] \
    && printf "%s\n" "$1" 
}

quit () {
  [ -n "$1" ] \
    && printf "error: %s\n" "$1"
  exit 1
}

ping_ip () {
  HOSTNAME_FQDN="$HOSTNAME_SHORT.$d"
  ping -c 2 $1 &> /dev/null
}

find_ip () {
  ping -c 2 $HOSTNAME_SHORT.$d | egrep -o '[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*' | head -1
}

echo $HOSTNAME_SHORT | egrep '\.' &> /dev/null \
  && quit "use short hostnames only"

for d in $DOMAINS; do
  HOST_IP="$(find_ip 2> /dev/null)"
  if [ -n "$HOST_IP" ]; then
    ping_ip $HOST_IP
    break
  fi
done

if [ -n "$HOSTNAME_FQDN" ]; then
  msg "${HOSTNAME_FQDN,,} [$HOST_IP]"
else
  msg "no FQDN found"
fi

exit 0
