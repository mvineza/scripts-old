#!/bin/sh
 
#change the directory to use, for example a folder created in downloads
workdir="/home/merrell/Downloads"

# Path where the binary of the theme is located, if it is the default, then the path is:
gst="/usr/share/gnome-shell/gnome-shell-theme.gresource"

# A folder is created in the path that the script.sh is executed, it is necessary to delete it, if they already have one, or create another one by changing the path of the workdir
cd $workdir
mkdir -p theme/org/gnome/shell/theme

# Extracting files from binary
for r in `gresource list $gst`; do
        #gresource extract $gst $r > $workdir ${r/#\/org\/gnome\/shell/}
        gresource extract $gst $r > theme/$r
done
