#!/bin/bash
#
# Simple script to backup /home/merrell using `tar`
# while using an exclude file to exclude garbages
#
# mvineza,rell.vineza14@yahoo.com
#
# REVISIONS:
#  mvineza - 07/09/2017 11:00 - copy backup file to merrellpc
#  mvineza - 07/14/2017 08:04 - added copy_failed()
#  mvineza - 07/26/2017 22:06 - simplified files to backup inside /etc and added fonts directory
#  mvineza - 07/29/2017 12:46 - simplified backup list to ~ and /etc
#

EXCLUDE_FILE="/home/merrell/Documents/notes/exclude.files"
TARGET_DATA="/home/merrell /etc"
BAK_FILE_FULL="/backups/myfedora-backup-full-$(date +%m-%d-%Y-%H-%M-%S).tgz"
BAK_FILE_INC="/backups/myfedora-backup-inc-$(date +%m-%d-%Y-%H-%M-%S).tgz"
SNAP_FILE="/backups/myfedora-snap-file"
REMOTE_PATH="/mnt/merrell/Tmp"
FAILED_COPIES="/home/merrell/Documents/notes/backup_myfedora.failed_copies"

quit () {
  [ -n "$1" ] \
    && printf "error: %s\n" "$1"
  sudo -u merrell zenity --error --text="$1" --display=:0
  exit 1
}

copy_failed () {
  echo $BAK_FILE >> $FAILED_COPIES
  quit "unable to copy $BAK_FILE"
}

mkdir /backups &> /dev/null

[ "$(id -u)" != "0" ] \
  && quit "run script as root"

if [ "$1" == "-f" ]; then
  echo "Backing up files .."
  BAK_FILE=$BAK_FILE_FULL
  tar cvfz $BAK_FILE \
    --exclude-from=$EXCLUDE_FILE \
    $TARGET_DATA \
    -g $SNAP_FILE \
    --level=0
elif [ "$1" == "-i" ]; then
  echo "Backing up files .."
  BAK_FILE=$BAK_FILE_INC
  tar cvfz $BAK_FILE \
    --exclude-from=$EXCLUDE_FILE \
    $TARGET_DATA \
    -g $SNAP_FILE
else
  echo "Usage:"
  echo "  -i -- incremental"
  echo "  -f -- full"
  exit 0
fi

echo
echo "Copying backup file to $REMOTE_PATH .."
cp -p $BAK_FILE $REMOTE_PATH/ 2> /dev/null \
  || copy_failed

echo
echo "Files excluded from backup:"
echo "--------------------------"
cat $EXCLUDE_FILE
echo
echo "backup file: $BAK_FILE"

exit 0
