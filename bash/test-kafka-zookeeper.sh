#!/bin/bash
#
# ================================================================ #
# Program: test-kafka-zookeeper.sh
#
# Description:
#   Tests kafka/zookeeper connection by creating a test topic and
#   consuming it.
#
# Author: Merrell Vineza (rell.vineza14@yahoo.com)
# Date: 03/27/2018 06:16:00 PHT
# ================================================================ #
#

# ================================================================ #
# DATA
# ================================================================ #

CMD_DIR="/usr/local/bin/kafka_2.11-1.0.0/bin"
TOPIC_NAME="$1"
NODE="$2"

# ================================================================ #
# FUNCTIONS
# ================================================================ #

# ================================================================ #
# MAIN
# ================================================================ #

if [[ -z "$TOPIC_NAME" || -z "$NODE" ]]; then
  echo "Usage: ./test-kafka-zookeeper.sh TOPIC_NAME NODE"
  exit 1
fi

$CMD_DIR/kafka-topics.sh --create --zookeeper $NODE:2181 --replication-factor 1 --partitions 1 --topic $TOPIC_NAME
$CMD_DIR/kafka-topics.sh --zookeeper $NODE:2181 --describe --topic $TOPIC_NAME
echo "hello" | $CMD_DIR/kafka-console-producer.sh --broker-list $NODE:9092 --topic $TOPIC_NAME
$CMD_DIR/kafka-console-consumer.sh --bootstrap-server $NODE:9092 --topic $TOPIC_NAME --from-beginning

exit 0
