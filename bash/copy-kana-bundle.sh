#!/bin/bash

FILE_LIST="/tmp/files.lst"
#SRC_HOST="mdc-kn-pkg01.msgreen.dom" # PKG SERVER
#SRC_HOST="mdc-ts-kn-app02.msgreen.dom" # TST2
#SRC_HOST="mdc-ts-kn-app03.msgreen.dom" # TST3
SRC_HOST="mdc-de-kn-app05.msgreen.dom" # TST4
ARTIFACTORY_HOST="trc-ptc-afact01.msred.dom"
BUNDLE_DIR="/home/kanauser/14R1releasebundle"
DOWNLOAD_DIR="/home/merrell/Downloads"

for file in `cat $FILE_LIST`; do
  echo "Copying $file to localhost .."
  scp root@$SRC_HOST:$BUNDLE_DIR/$file $DOWNLOAD_DIR
  echo

  echo "Uploading $file to artifactory .."
  scp $DOWNLOAD_DIR/$file mvineza@$ARTIFACTORY_HOST:~/
  ssh mvineza@$ARTIFACTORY_HOST "curl -X PUT -u kana:M@kati2014 -T $file http://localhost:8081/artifactory/kana/$file"
  #curl -X PUT -u kana:M@kati2014 -T ${DOWNLOAD_DIR}/${file} http://${ARTIFACTORY_HOST}:8081/artifactory/kana/${file}
  echo
done

exit 0
