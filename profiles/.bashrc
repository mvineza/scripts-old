# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=-1
HISTFILESIZE=-1

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
    # PS1="\[\e]0;rel~gel: \w\a\]${debian_chroot:+($debian_chroot)}\[\033[01;32m\]rel\[\033[38;5;160m\]💗\[\033[01;32m\]gel\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ " # rel~gel
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# ---------- Start copying here ---------- #
#
# User specific variables
export HISTTIMEFORMAT='%m-%d-%Y %H:%M:%S '
#
# User specific aliases and functions
#
# ssh aliases
alias vm03='ssh -qo "StrictHostKeyChecking no" -p 2212 root@127.0.0.1'
alias vm02='ssh -qo "StrictHostKeyChecking no" -p 2211 root@127.0.0.1'
alias vm01='ssh -qo "StrictHostKeyChecking no" -p 2234 root@127.0.0.1'
alias client01='ssh -qo "StrictHostKeyChecking no" -p 2211 root@127.0.0.1'
alias server01='ssh -qo "StrictHostKeyChecking no" -p 2210 root@127.0.0.1'
alias tower-innove-tst='ssh -qo "StrictHostKeyChecking no" mvineza@mic-tst-itass01.msgreen.dom'
alias tower-innove-stg='ssh -qo "StrictHostKeyChecking no" mvineza@msc-co-itass01.msorange.dom'
alias tower-innove-prd='ssh -qo "StrictHostKeyChecking no" mvineza@mic-co-itass01.msred.dom'
alias tower-taiwan-stg='ssh -qo "StrictHostKeyChecking no" mvineza@trc-sct-itass01.msorange.dom'
alias tower-taiwan-prd='ssh -qo "StrictHostKeyChecking no" mvineza@trc-ptc-itass01.msred.dom'
alias tower-taiwan-alpineprd='ssh -qo "StrictHostKeyChecking no" mvineza@trc-co-itass01.ph.esl-asia.com'
alias tower-ipvg-prd='ssh -qo "StrictHostKeyChecking no" mvineza@iic-co-itass01.ph.esl-asia.com'
alias spacewalk-innove-stg='ssh -qo "StrictHostKeyChecking no" mvineza@mic-stg-spwlk01.msorange.dom'
alias testing-rm-01='ssh -qo "StrictHostKeyChecking no" mvineza@mtc-esb-rm01.msgreen.dom'
alias testing-rm-02='ssh -qo "StrictHostKeyChecking no" mvineza@mtc-esb-rm02.msgreen.dom'
alias kana-tst2='ssh root@mdc-ts-kn-app02.msgreen.dom'
alias kana-tst3='ssh root@mdc-ts-kn-app03.msgreen.dom'
alias kana-tst4='ssh root@mdc-de-kn-app05.msgreen.dom'
#
# terminal aliases
#
alias ll='ls -l --color=auto'
alias mt='mate-terminal --tab -t $1'
alias scg='source ~/.bashrc_git'
alias tmc='tmux new script $MY_TYPESCRIPT'
alias swdns='sudo cp -f /etc/resolv.conf.multi /etc/resolv.conf'
alias krestart='kquitapp5 plasmashell && kstart plasmashell &> /dev/null'
alias krestart5='pkill -9 plasmashell; kstart5 plasmashell &> /dev/null'
#
# tmux aliases
alias tpy='tmux new-session python2\; split-window -v python3\; select-pane -t 0 \; set-window-option synchronize-panes \; attach'
alias bi3='tmux new-session sudo su - ansible\; split-window -d sudo su - ansible\; split-window -d sudo su - ansible\; select-pane -t 0\; set-window-option synchronize-panes \; select-layout even-vertical\; attach'
#
# my cool bash prompts
# export PS1="\[\033[38;5;94m\][\[$(tput sgr0)\]\[\033[38;5;127m\]\W\[$(tput sgr0)\]\[\033[38;5;94m\]]_\[$(tput sgr0)\]\[\033[38;5;2m\]\\$\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]"
# PS1="\[\e[1;34m\]$\[\e[0m\] " # disable this and use KDE's PS1

# functions

# Set terminal title
function set-title(){
  if [[ -z "$ORIG" ]]; then
    ORIG="$PS1"
  fi
  TITLE="\[\e]2;$*\a\]"
  PS1="${ORIG}${TITLE}"
}

# Removes bridge connections which can be used after
# docker restart
function rmbridge() {
  sudo nmcli con down `nmcli con show | grep br- | awk '{print $4}'`
}

# For google-cloud sdk
if [ -f '/usr/local/bin/google-cloud-sdk/path.bash.inc' ]; then source '/usr/local/bin/google-cloud-sdk/path.bash.inc'; fi
if [ -f '/usr/local/bin/google-cloud-sdk/completion.bash.inc' ]; then source '/usr/local/bin/google-cloud-sdk/completion.bash.inc'; fi

# virtualenv
export WORKON_HOME=~/.virtualenvs
source virtualenvwrapper.sh \
  || echo "virtualenv missing, install by: pip install virtualenv virtualenvwrapper"

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/merrell/google-cloud-sdk/path.bash.inc' ]; then . '/home/merrell/google-cloud-sdk/path.bash.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/merrell/google-cloud-sdk/completion.bash.inc' ]; then . '/home/merrell/google-cloud-sdk/completion.bash.inc'; fi
