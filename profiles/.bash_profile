# .bash_profile
#
# All of these below are executed during login shells.
# What are login shells?
#  a. you type your username and password on the console infront of the machine
#  b. you connect remotely via SSH to the machine (ofcourse using credentials again)
#

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs
export PATH=$PATH:$HOME/.local/bin:$HOME/bin
export MY_TYPESCRIPT="/home/merrell/.typescripts/typescript.$(date +"%m-%d-%Y-%H-%M-%S")"
export PYTHONSTARTUP=$HOME/.pythonrc.py

# My startup scripts for login shells
# tmux new script $MY_TYPESCRIPT
# script $MY_TYPESCRIPT # fedora 26 Xorg logins hangs when this is enabled
# xhost &> /dev/null || script $MY_TYPESCRIPT # this fixes the above issue
