#!/usr/bin/env python
"""
Small script to run another program and time its execution.

This measures the elapsed time (in seconds) from start the program run until
it ends. The output in this program is in seconds and might slightly differ on
the elapsed time output of the program you are measuring (if it has any).
"""

__author__ = "Merrell Vineza"
__date__ = "1/12/2017 13:13:06 PHT"
__version__ = "1.0.0"
__email__ = "rell.vineza14@yahoo.com"

from sys import argv, exit
from time import time
import subprocess

command = argv[1:]


if len(argv) == 1:
    print('Usage: {} command'.format(argv[0]))
    exit(1)
print('Executing: {}'.format(" ".join(command)))
start = time()
try:
    subprocess.call(command)
    end = time()
    print('Elapsed time: {} seconds'.format(end - start))
except:
    print('Error executing command!')
    exit(1)
