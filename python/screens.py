#!/usr/bin/env python3
"""
Gets screenshots and save them to the specified directory.
"""

__author__ = "Merrell Vineza"
__date__ = "10/15/2017 17:53:38"
__version__ = "1.0.0"
__email__ = "rell.vineza14@yahoo.com"


# TODO:
#
#  - test this in fedora
#  - make hsbc() work
#  - make screenshots in linux more clear
#  - avoid cleartext password
#  - pop up message when exception occured
#  - store passwords on separate file
#  - replace zip commands with a python module
#      * this might eliminate storing password on every as `sys.platform` type
#  - simplify try-except statements at the last part
#

from selenium import webdriver
from pyvirtualdisplay import Display
import time
import os
import sys
from subprocess import call
try:
    from creds import screens
except ImportError:
    lib = "\n".join(sys.path)
    print('ERROR: Unable to find credentials from: \n\n{}'.format(lib))
    sys.exit(1)

# ================ #
# global variables #
# ================ #

timestamp = time.strftime("-%m-%d-%Y-%H-%M-%S")
image_name_temp = 'temp.png'
file_extension = '.png'
home = os.environ['HOME']
screenshots_path = os.path.join(home, 'Pictures/Screenshots')

# ====================== #
# os dependent variables #
# ====================== #

if sys.platform == 'linux':
    archive_prog = '/usr/bin/zip'
    archive_option = '-mP'
    archive_os_pass = screens.archive_pass
elif sys.platform == 'win32':
    archive_prog = r'C:\Program Files\WinRAR\WinRAR.exe'
    archive_option = 'm'
    archive_os_pass = '-p' + screens.archive_pass
else:
    print('cannot determine platform')
    sys.exit(1)

# ============= #
# bpi variables #
# ============= #

bpi_screenshot_archive_file = 'bpi_screenshots.zip'
bpi_url = ("https://secure1.bpiexpressonline.com/AuthFiles/"
           "login.aspx?URL=/direct_signin.htm")
bpi_timeout = 10

# ============== #
# rcbc variables #
# ============== #

rcbc_screenshot_archive_file = 'rcbc_screenshots.zip'
rcbc_url = 'https://www.rcbconlinebanking.com/web/lg/Login/Index'
rcbc_timeout = 10

# ==================== #
# union bank variables #
# ==================== #

ub_screenshot_archive_file = 'ub_screenshots.zip'
ub_url = ("https://ebanking.unionbankph.com/personal"
          "/AuthenticationController?__START_TRAN_FLAG__=Y&FORMSGROUP_ID__="
          "AuthenticationFG&__EVENT_ID__=LOAD&FG_BUTTONS__=LOAD&ACTION.LOAD="
          "Y&AuthenticationFG.LOGIN_FLAG=1&BANK_ID=01&LANGUAGE_ID=001")
ub_timeout = 10

# ============== #
# hsbc variables #
# ============== #

hsbc_screenshot_archive_file = 'hsbc_screenshots.zip'
hsbc_url = 'https://www.hsbc.com.ph/'
hsbc_timeout = 10

def initialize_browser():
    global browser
    if sys.platform == 'linux':
        display = Display(visible=0, size=(1920, 1080))
        display.start()
    browser = webdriver.Chrome()
    os.chdir(screenshots_path)


def rename_image(screen_prefix, zip_file):
    old_name = image_name_temp
    new_name = screen_prefix + timestamp + '.png'
    os.rename(old_name, new_name)
    add_to_archive(zip_file, new_name)


def add_to_archive(archive, file):
    call([archive_prog, archive_option, archive_os_pass, archive, file])


def ub_virt_keyboard(key):
    browser.find_element_by_xpath(key).click()


def bpi_login():
    initialize_browser()
    browser.get(bpi_url)
    browser.implicitly_wait(bpi_timeout)  # enhancement: make this a one liner
    user = browser.find_element_by_name('UserID')
    user.send_keys(screens.bpi_username)
    # enhancement: make this a one liner
    password = browser.find_element_by_name('Password')
    password.send_keys(screens.bpi_password)
    login = browser.find_element_by_name('sSubmit')
    login.click()
    browser.save_screenshot(image_name_temp)
    rename_image('bpi', bpi_screenshot_archive_file)
    browser.quit()  # enhancement: replace this with correct logout action


def rcbc_login():
    initialize_browser()
    browser.get(rcbc_url)
    browser.implicitly_wait(rcbc_timeout)
    try:
        browser.find_element_by_xpath("//button[@id='ok']").click()
    # TODO: put a correct exception here
    except:
        pass
    browser.find_element_by_name('UserId').send_keys(screens.rcbc_username)
    browser.find_element_by_name('Password').send_keys(screens.rcbc_password)
    browser.find_element_by_css_selector('#btnLogin').click()
    browser.find_element_by_css_selector('#secQues').click()
    enter_answer = browser.find_element_by_xpath(
        "//input[@id='ChallengeAns']")
    sec_question = browser.find_element_by_xpath(
        "//span[@id='lblChallengeQuestion']").text
    while sec_question == 'Loading Security Question. Please wait...':
        sec_question = browser.find_element_by_xpath(
            "//span[@id='lblChallengeQuestion']").text
    if sec_question == screens.rcbc_sec_ques1:
        enter_answer.send_keys(screens.rcbc_sec_ans1)
    elif sec_question == screens.rcbc_sec_ques2:
        enter_answer.send_keys(screens.rcbc_sec_ans2)
    elif sec_question == screens.rcbc_sec_ques3:
        enter_answer.send_keys(screens.rcbc_sec_ans3)
    else:
        print(sec_question, ': Security question not on list')
    browser.find_element_by_css_selector('#btnSubmitModal').click()
    browser.find_element_by_class_name('account').click()
    time.sleep(5)  # enhancement: replace this with a better solution
    browser.save_screenshot(image_name_temp)
    rename_image('rcbc', rcbc_screenshot_archive_file)
    browser.find_element_by_xpath("//a[@id='_logout']").click()
    browser.find_element_by_xpath("//button[@id='YESButton']").click()
    browser.quit()


def ub_login():
    initialize_browser()
    browser.get(ub_url)
    browser.implicitly_wait(ub_timeout)
    browser.find_element_by_name('USER_ID_TEMP').send_keys(screens.ub_username)
    browser.find_element_by_name('AuthenticationFG.ACCESS_CODE').click()
    ub_virt_keyboard("//li[@class='left-shift first']")
    ub_virt_keyboard("//span[text()='{}']".format(screens.ub_pass_1))
    ub_virt_keyboard("//span[text()='ABC']")
    ub_virt_keyboard("//li[@class='first capslock row2']")
    ub_virt_keyboard("//span[text()='{}']".format(screens.ub_pass_2))
    ub_virt_keyboard("//li[@class='first capslock row2']")
    ub_virt_keyboard("//span[text()='{}']".format(screens.ub_pass_3))
    ub_virt_keyboard("//span[text()='{}']".format(screens.ub_pass_4))
    ub_virt_keyboard("//li[@class='left-shift first']")
    ub_virt_keyboard("//span[text()='{}']".format(screens.ub_pass_5))
    ub_virt_keyboard("//span[text()='ABC']")
    ub_virt_keyboard("//span[text()='{}']".format(screens.ub_pass_6))
    ub_virt_keyboard("//span[text()='{}']".format(screens.ub_pass_7))
    ub_virt_keyboard("//span[text()='{}']".format(screens.ub_pass_8))
    ub_virt_keyboard("//span[text()='{}']".format(screens.ub_pass_9))
    browser.find_element_by_name('Action.VALIDATE_CREDENTIALS').click()
    browser.save_screenshot(image_name_temp)
    rename_image('ub', ub_screenshot_archive_file)
    browser.find_element_by_name('HREF_HREF_Logout').click()
    browser.find_element_by_name('HREF_LOG_OUT').click()
    browser.quit()


def hsbc_login():
    initialize_browser()
    browser.get(hsbc_url)
    browser.fullscreen_window()
    browser.implicitly_wait(hsbc_timeout)
    browser.find_element_by_class_name('hsbcImageStyle08').click()
    browser.find_element_by_name('userid').send_keys(screens.hsbc_username)
    browser.find_element_by_class_name('submit_input').click()
    browser.find_element_by_xpath("//li[@aria-checked='false']").click()
    browser.find_element_by_name('memorableAnswer').send_keys(
        screens.hsbc_memorable_answer)
    for i in screens.hsbc_pass:
        try:
            browser.find_element_by_xpath(
                "//input[@id='{0}']".format(i)).send_keys(screens.hsbc_pass[i])
        except:
            print('hsbc: unable to find id {0}'.format(i))
    browser.find_element_by_class_name('submit_input').click()
    time.sleep(10)  # enhancement: replace this with a better solution
    browser.save_screenshot(image_name_temp)
    rename_image('hsbc', hsbc_screenshot_archive_file)
    browser.find_element_by_xpath("//a[@title='Log Off']").click()
    browser.quit()


try:
    hsbc_login()
except:
    print('Unable to get hsbc screenshots')
try:
    ub_login()
except:
    print('Unable to get union bank screenshots')
try:
    rcbc_login()
except:
    print('Unable to get rcbc screenshots')
try:
    bpi_login()
except:
    print('Unable to get bpi screenshots')
