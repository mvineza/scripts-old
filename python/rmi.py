#!/usr/bin/env python
"""
Simple script to delete docker images.
"""

__author__ = "Merrell Vineza"
__date__ = "02/12/2018 21:47:05 PHT"
__version__ = "1.0.0"
__email__ = "rell.vineza14@yahoo.com"

import click
from os import system
from re import findall
from subprocess import check_output


def get_images(image):
    res = check_output('sudo docker images | grep {}'.format(image), shell=True)
    ids = findall('[a-z0-9]{12,12}', res)
    return ids


@click.command()
@click.option('image', '-i', required=True, help='image name')
def del_image(image):
    img_ids = get_images(image)
    for i in img_ids:
        print('Deleting {} ..'.format(i))
        system('sudo docker rmi -f {}'.format(i))


if __name__ == '__main__':
    del_image()
