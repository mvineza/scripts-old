#!/usr/bin/env python

from __future__ import print_function
import socket
from argparse import ArgumentParser

"""
Simple program to transfer a file on a remote host via a custom port.
"""

__author__ = "Merrell Vineza"
__date__ = "11/04/2017 19:00:00 PHT"
__version__ = "1.0.0"
__email__ = "rell.vineza14@yahoo.com"


def get_options():
    parser = ArgumentParser()
    parser.add_argument('-H', metavar='hostname',
                        default=None, help='remote host', required=True)
    parser.add_argument('-p', metavar='port',
                        default=None, help='remote port', required=True)
    parser.add_argument('-f', metavar='file',
                        default=None, help='file to send', required=True)
    return parser.parse_args()


def send_file(host, port, file):
    s = socket.socket()
    print('Establishing connection to host ..')
    s.connect((host, port))
    datatosend = open(file, 'rb').read()
    size = len(datatosend)
    print('File size: {}'.format(size))
    print('Sending file ..')
    totalsent, sent = 0, 0
    while size > 0:
        sent = s.send(datatosend[totalsent:])
        if sent == 0:
            print('Unable to send further data, quiting ..')
            break
        # For the time-being, I commented this out because it doesn't produce
        # the correct value. So better to use a separate variable for now as a
        # placeholder for total bytes sent.
        # sent += s.send(datatosend[sent:])
        totalsent += sent
        size -= sent
        print('Total bytes sent: {}'.format(totalsent))
    s.close()


def main():
    args = get_options()
    file = args.f
    host = args.H
    port = int(args.p)
    send_file(host, port, file)


if __name__ == '__main__':
    main()
