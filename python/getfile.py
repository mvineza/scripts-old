#!/usr/bin/env python

from __future__ import print_function
import socket
from sys import exit
from os import stat
from os.path import abspath
from argparse import ArgumentParser

"""
Simple program which receives a file from a particular port.
"""

__author__ = "Merrell Vineza"
__date__ = "11/04/2017 19:00:00 PHT"
__version__ = "1.0.0"
__email__ = "rell.vineza14@yahoo.com"


def get_options():
    parser = ArgumentParser()
    parser.add_argument('-p', metavar='port', help='port to receive the file',
                        required=True)
    parser.add_argument('-f', metavar='file', help='destination filename',
                        required=True)
    return parser.parse_args()


def setup(file):
    try:
        stat(file)
        print('File exists: {}'.format(abspath(file)))
        exit(1)
    except OSError:
        pass


def listen(port, file):
    s = socket.socket()
    print('Ready to receive ..')
    s.bind(('', port))
    s.listen(1)
    global clientsocket
    clientsocket, addr = s.accept()
    if clientsocket:
        print('Client connected: {}'.format(addr))
    filesize = 0
    with open(file, 'wb') as f:
        while True:
            data = clientsocket.recv(8192)
            if len(data) == 0:
                print('connection closing ..')
                clientsocket.shutdown(1)
                clientsocket.close()
                break
            else:
                filesize += len(data)
                print('{} bytes received ..'.format(filesize))
                f.write(data)
    print('File downloaded: {}'.format(abspath(file)))


def main():
    try:
        args = get_options()
        file = args.f
        port = int(args.p)
        setup(file)
        listen(port, file)
    except KeyboardInterrupt:
        exit(1)


if __name__ == '__main__':
    main()
