#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 Copyright (c) 2017 ESL Automation team

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

from __future__ import print_function
from pyVim import connect
from pyVmomi import vim
import ssl
import pchelper
import re
import click
import os

__author__ = "IT Ops Linux ESL"
__contact__ = "merrell.vineza@esl-asia.com"
__version__ = ": 1.0 $"
__date__ = ": 2017/10/06 $"
__copyright__ = "Copyright (c) 2017 ESL Automation team"
__license__ = "Python"


class FindVM(object):
    def __init__(self, hostname, username, password, project):
        self.project = project
        self.si = None
        self.context = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
        self.context.verify_mode = ssl.CERT_NONE
        self.si = connect.SmartConnect(host=hostname,
                                       user=username,
                                       pwd=password,
                                       sslContext=self.context)
        if not self.si:
            raise SystemExit("Unable to connect to host with supplied info.")

    def get_attributes(self):
        vm_properties = ["name", "summary.customValue"]
        view = pchelper.get_container_view(self.si,obj_type=[vim.VirtualMachine])
        vm_data = pchelper.collect_properties(self.si,
                                              view_ref=view,
                                              obj_type=vim.VirtualMachine,
                                              path_set=vm_properties,
                                              include_mors=True)
        vm_discovered = []
        for vm in vm_data:
            match = re.findall('value = \'.*\'', str(vm))
            for p in self.project.split(','):
                try:
                    val = match[0].split('=')[1].strip().strip('\'')
                except:
                    continue
                for v in val.split(','):
                    v1 = re.search('^{}$'.format(p), v)
                    if v1:
                        vm_discovered.append(((vm["name"]).lower()).split('.')[0])
        return set(vm_discovered)

def import_pwd():
    pwd_file = os.path.join(os.environ['HOME'], '.search_vm_pass')
    f = open(pwd_file)
    pwd = (f.readlines())[0].strip()
    f.close()
    return pwd


@click.command()
@click.option('hostname', '-H', help='vcenter hostname', required=True)
@click.option('username', '-u', default='mvineza', help='vcenter username', required=True)
@click.option('password', '-p', default=import_pwd(), help='vcenter password', required=True)
@click.option('project', '-P', help='project to query', required=True)
def request_vm(hostname,username,password,project):
    service_instance = FindVM(hostname, username, password, project)
    vms = service_instance.get_attributes()
    print("\n".join(vms))


if __name__ == '__main__':
  request_vm()
