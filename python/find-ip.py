#!/usr/bin/env python3
#
# TODO:
#   - adapt script for windows cmd that doesn't handle ansi colors
#   - add a preferrable description and history revisions
#   - auto adjust alignment for multiple outputs
#   - allow use of delimiter `-`, `--`, and so on..
#
"""
Returns IP of host(s) being queried. This relies on `socket.gethostname()` method
which makes the IP resolution very fast.
"""

__author__ = "Merrell Vineza"
__date__ = "09/02/2017"
__version__ = "1.0.0"
__email__ = "rell.vineza14@yahoo.com"

from argparse import ArgumentParser
from socket import gethostbyname, gethostbyaddr
from sys import exit

def get_single_ip(host, delimiter):
  try:
    ip = gethostbyname(host)
    print(white + host + ' ' + delimiter + ' ' + green + ip)
  except:
    ip = 'can\'t resolve hostname'
    print(white + host + ' ' + delimiter + ' ' + red + ip)

def get_multi_ip(file, delimiter):
  with open(file) as f:
    hosts = f.readlines()
    for h in hosts:
      get_single_ip(h.strip(), delimiter)

def get_reverse_dns(ip):
  try:
    result = gethostbyaddr(ip)
  except:
    print(red + 'unable to map IP to a hostname')
    exit(1)
  hostname = result[0]
  alias = result[1]
  print('Hostname :', hostname)
  print('Alias    :', ", ".join(alias))

parser = ArgumentParser(description='returns IP of specified host')
group = parser.add_mutually_exclusive_group()
group.add_argument('-H', dest='host', help='target host')
group.add_argument('-f', dest='file', help='file containing host(s)')
group.add_argument('-i', dest='ip', help='displays hostname and alias(es) of specified IP')
parser.add_argument('-d', dest='delimiter', default='=', help='custom separator between host and ip')

args = parser.parse_args()

host = args.host
file = args.file
delimiter = args.delimiter
ip = args.ip

green = '\033[1;32;49m'
red = '\033[1;31;49m'
white = '\033[1;37;49m'

if host:
  get_single_ip(host, delimiter)
elif file:
  get_multi_ip(file, delimiter)
elif ip:
  get_reverse_dns(ip)
else:
  parser.print_help()
  exit(1)
