#-*-encoding:utf-8-*-
"""
r1   : Première version très naïve, poc.
r2   : Traitement des modules n'étant pas dans le dossier courant
r3   : Traitement des dossiers (tous les fichiers python d'un dossier).
r4   : ? 
r5   : ? 
r6   : docstrings handling
r7   : correct handling of directories
r8   : correct handling of single files (bug probably introduced in r7)
r9.0 : gle stats

Nb of modules : 12
+ Module : mod1 => 102 loc
`--+ 306 functions => 3940 loc
   `-- function 1 : 20 loc
`--+ 49 classes => 2048 loc
   `--+ class MyClass => 92 loc
      `-- method1 : 30 loc
      `-- method2 : 38 loc

r9.1  : printing by + module
                 |
                 `-- functions
                 |
                 `-- classes
                  |
                  `-- methods

       printing gl and avg statistics

r9.2 : printing max statistics
r10  : refactoring many functions (slightly simplified)
r11  : print the name of module/module+class when printing the longest function/method.
r12  : fix the bug where modules are imported, by using inspect.getmodule(item)
r13  : fix absolute path arguments crashing the program (can't import)
r14  : fix the bug about functions counting (used to be 0)


         Past
          ^
          |
 ------ present ----- r15 : Use the operator module to replace handwritten functions get_second_subitem by operator.itemgetter
          |
          v
        future


r14  : accept a --verbose option to enable/disable info prints
r15  : show class inheritences
"""

import inspect
import sys
import os
import dircache
import pprint
import subprocess
import functools
import operator

class Inspector(object):

    def __init__(self,modules_names,max_length=25,stop_at_warning=False):
        self.modules_names        = modules_names
        self.max_length           = max_length
        self.loc                  = 0
        self.all_ok               = True
        self.stop_at_warning      = stop_at_warning
        # self.stats looks like this
        #   { module_name     : { "functions" : { "#nb_functions":N,
        #                                         "#loc",N
        #                                         function_name:N,
        #                                       },
        #                         "classes"   : { "#nb_classes":N,
        #                                         "#loc":N,
        #                                         "methods": {method_name:N}
        #                                       }
        #                       },
        #     "#loc"          : N,
        #     "#nb_classes"   : N,
        #     "#nb_functions" : N,
        #     "#nb_methods"   : N
        #     }
        self.stats                = {}

    def print_stats(self):
        self.print_modules_stats()
        self.print_gl_stats()
        self.print_avg_stats()
        self.print_max_stats()

    def print_gl_stats(self):
        print "Number of modules %s" % self.stats["#nb_modules"]
        output =  "Total : %s loc across %s modules containing %s functions, %s classes and %s methods."
        tup = tuple(self.stats[key] for key in ('#loc','#nb_modules','#nb_functions','#nb_classes','#nb_methods'))
        print output % tup

    def print_avg_stats(self):
        if self.stats["#nb_functions"]:
            self.print_avg_functions_stats()
        if self.stats["#nb_classes"]:
            self.print_avg_methods_stats()
            self.print_avg_classes_stats()
        self.print_avg_modules_stats()

    def print_avg_functions_stats(self):
        nb_functions  = self.stats["#nb_functions"]
        functions_loc = sum([self.stats[module_name]["functions"]["#loc"] for module_name in self.stats if is_dict_keyword(module_name)])
        func_avg_loc  = functions_loc * 1.0 / nb_functions
        print "avg loc per function : %s" % func_avg_loc

    def print_avg_methods_stats(self):
        nb_methods      = self.stats["#nb_methods"]
        methods_loc     = sum([self.stats[module_name]["classes"]["#loc"] for module_name in self.stats if is_dict_keyword(module_name)])
        methods_loc_avg = 1.0 * methods_loc / nb_methods
        print "avg loc per method   : %s" % methods_loc_avg

    def print_avg_classes_stats(self):
        class_loc = sum([self.stats[module]["classes"][classname]["#loc"]
                         for module in self.stats
                         if is_dict_keyword(module)
                         for classname in self.stats[module]["classes"]
                         if is_dict_keyword(classname)
                         ])
        nb_classes = self.stats["#nb_classes"]
        class_avg_loc = 1.0 * class_loc / nb_classes
        print "avg loc per class    : %s" % class_avg_loc

    def print_avg_modules_stats(self):
        nb_modules = self.stats["#nb_modules"]
        module_loc = 1.0 * self.stats["#loc"] / nb_modules
        print "avg loc per module   : %s" % module_loc

    def print_max_stats(self):

        if self.stats["#nb_functions"]:
            function_name,loc_function = self.get_max_function()
            print "longest function     : %s with %s loc" % (function_name , loc_function )
            
        if self.stats["#nb_classes"]:
            method_name,loc_method     = self.get_max_method()
            class_name,loc_class       = self.get_max_class()
            print "longest method       : %s with %s loc" % (method_name   , loc_method   )
            print "longest class        : %s with %s loc" % (class_name    , loc_class    )
        
        module_name,loc_module = self.get_max_module()
        print "longest module       : %s with %s loc" % (module_name,loc_module)

    def get_max_function(self):
        # list of dicts, one dict per module
        functions        = [(module,function_name,loc)
                            for module in self.stats.keys()
                            if is_dict_keyword(module)
                            for (function_name,loc) in self.stats[module]["functions"].iteritems()
                            if is_dict_keyword(function_name)
                            ]
        # get the biggest pair by looking at the second element which is loc
        longest_function = max(functions,key=lambda x:x[2])
        return longest_function[0]+"."+longest_function[1],longest_function[2]
    
    def get_max_method(self):
        methods = [(module,class_name,method_name,loc)
                   for module in self.stats.keys()
                   if  is_dict_keyword(module)
                   for class_name in self.stats[module]["classes"]
                   if  is_dict_keyword(class_name)
                   for (method_name,loc) in self.stats[module]["classes"][class_name]["methods"].iteritems()
                   if is_dict_keyword(method_name)
                   ]
        max_method = max(methods,key=lambda x:x[3])
        
        return ".".join(max_method[0:3]),max_method[3]

    def get_max_class(self):
        classes = [(module, class_name,self.stats[module]["classes"][class_name]["#loc"])
                   for module in self.stats if is_dict_keyword(module)
                   for class_name in self.stats[module]["classes"] if is_dict_keyword(class_name)
                   if is_dict_keyword(class_name)
                   ]
        module,class_name,loc = max(classes,key=lambda x:x[2])
        return ".".join((module,class_name)),loc

    def get_max_module(self):
        return max([(module_name, self.stats[module_name]["#loc"]) for module_name in self.stats if is_dict_keyword(module_name)],
                   key = operator.itemgetter(1))
        
    
    def print_modules_stats(self):
        for module_name in filter(is_dict_keyword,self.stats):
            self.print_module_stats(module_name)
        
    def print_module_stats(self,module_name):
        output       = "+ Module %s => %s loc"
        print output % (module_name,self.stats[module_name]["#loc"])
        self.print_functions_stats(module_name)
        self.print_class_stats(module_name)

    def print_functions_stats(self,module_name):
        nb_functions = self.stats[module_name]["functions"]["#nb_functions"]
        loc          = self.stats[module_name]["functions"]["#loc"]
        print_branch(0,"+ %s functions => %s loc" % (nb_functions,loc))
        functions_names     = self.stats[module_name]["functions"]
        longest_name_length = max(map(lambda x:len(x),functions_names))
        lnl                 = longest_name_length
        for function_name in filter(is_dict_keyword,functions_names):
            print_branch(3," "+function_name.ljust(lnl) + " => %s loc" % self.stats[module_name]["functions"][function_name])

    def print_class_stats(self,module_name):
        print_branch(0,"+%s classes => %s loc" % (self.stats[module_name]["classes"]["#nb_classes"],self.stats[module_name]["classes"]["#loc"]))
        for class_name in filter(is_dict_keyword,self.stats[module_name]["classes"]):
            nb_methods = self.stats[module_name]["classes"][class_name]['methods']["#nb_methods"]
            loc        = self.stats[module_name]["classes"][class_name]["#loc"]
            print_branch(3,"+ class %s => %s methods, %s loc" % (class_name,nb_methods,loc))
            longest_name_length = max([len(name) for name in self.stats[module_name]["classes"][class_name]['methods']])
            for method_name in filter(is_dict_keyword,self.stats[module_name]["classes"][class_name]["methods"]):
                loc = self.stats[module_name]["classes"][class_name]['methods'][method_name]
                print_branch(6," %s : %s " % (method_name.ljust(longest_name_length),loc))
                
    def process_modules(self):
        """
        main function of this class.
        for every directory :
            for every module in that directory
                1. scan the module's functions and count their loc
                2. for every class in current module :
                     scan that class's methods and count their loc
                     
        Each statistic is added locally and incremented in the parent's statistic.
        For example, each method adds its loc to the classes loc, the module's loc and the total loc.
        """

        self.init_global_stats()

        for module_name in self.modules_names:
            # module_name could be a directory_name, therefor, we loop.
            for module in get_module(module_name):
                functions = get_functions(module)
                classes   = get_classes(module)
                self.init_module_stats(module.__name__)
                for function_name, loc in self.process_callables(functions):
                    self.add_functions_loc(module.__name__,function_name,loc)

                for klass in classes :
                    methods = get_methods(klass)
                    self.init_class_stats(module.__name__,klass.__name__)
                    for method_name,loc  in self.process_callables(methods):
                        self.add_methods_loc(module.__name__,klass.__name__,method_name,loc)

    def init_global_stats(self):
        self.stats["#nb_modules"]   = 0
        self.stats["#nb_functions"] = 0
        self.stats["#nb_methods"]   = 0
        self.stats["#nb_classes"]   = 0
        self.stats["#loc"]          = 0

    def init_module_stats(self,module_name):
        #---------------------------------------------------------#                
        # global
        self.stats["#nb_modules"]                            += 1
        #---------------------------------------------------------#        
        # general
        self.stats[module_name]                               = {}
        self.stats[module_name]["#loc"]                       = 0
        self.stats[module_name]["#nb_classes"]                = 0
        self.stats[module_name]["#nb_methods"]                = 0
        self.stats[module_name]["#nb_classes"]                = 0
        self.stats[module_name]["#nb_functions"]              = 0          
        #---------------------------------------------------------#        
        # functions
        self.stats[module_name]["functions"]                  = {}
        self.stats[module_name]["functions"]["#loc"]          = 0
        self.stats[module_name]["functions"]["#nb_functions"] = 0
        #---------------------------------------------------------#        
        # classes
        self.stats[module_name]["classes"]                    = {}
        self.stats[module_name]["classes"]["#loc"]            = 0
        self.stats[module_name]["classes"]["#nb_classes"]     = 0
        #---------------------------------------------------------#
        
    def add_functions_loc(self,module_name,function_name,loc):
        self.stats[module_name]["functions"][function_name]    = loc
        self.stats[module_name]["functions"]["#loc"]          += loc
        self.stats[module_name]["#loc"]                       += loc
        self.stats["#loc"]                                    += loc
        #---------------------------------------------------------#        
        self.stats[module_name]["functions"]["#nb_functions"] += 1
        self.stats["#nb_functions"]                           += 1

    def init_class_stats(self,module_name,class_name):
        self.stats[module_name]["classes"]["#nb_classes"]                       += 1
        self.stats["#nb_classes"]                                               += 1
        #-----------------------------------------------------------------------------#                
        self.stats[module_name]["classes"][class_name]                           = {}
        self.stats[module_name]["classes"][class_name]["#loc"]                   = 0
        self.stats[module_name]["classes"][class_name]["methods"]                = {}
        #-----------------------------------------------------------------------------#
        self.stats[module_name]["classes"][class_name]["methods"]["#nb_methods"] = 0
        self.stats[module_name]["classes"]["#nb_methods"]                        = 0
        
    def add_methods_loc(self,module_name,class_name,method_name,loc):
        self.stats[module_name]["classes"][class_name]["methods"][method_name]   = loc
        self.stats[module_name]["classes"][class_name]["#loc"]                   += loc
        self.stats[module_name]["classes"]["#loc"]                               += loc
        self.stats[module_name]["#loc"]                                          += loc
        self.stats["#loc"]                                                       += loc
        #-----------------------------------------------------------------------------#
        self.stats[module_name]["classes"][class_name]["methods"]["#nb_methods"] += 1
        self.stats[module_name]["classes"]["#nb_methods"]                        += 1
        self.stats["#nb_methods"]                                                += 1
        
    def process_callables(self,callables):
        for kallable in callables:
            loc = count_loc(inspect.getsourcelines(kallable)[0])
            yield kallable.__name__,loc
        
def print_branch(depth,s):
    print (depth != 0 and '|' or '') + " "*depth+"`--"+s

def format_branch(depth,s):
    return (depth != 0 and '|' or '') + " "*depth+"`--"+s

def add_to_path(dir_name):
    if dir_name not in sys.path:
        sys.path.append(dir_name)
        print "added '%s' to the path" % (dir_name)
    
def get_module(file_name):
    # if it's a directory return all modules and submodules recursively
    if os.path.isdir(file_name):
        dir_name = file_name
        #add_to_path(dir_name)
        files = dircache.listdir(dir_name)
        for one_file in files:
            complete_file_name = os.path.join(dir_name,one_file)
            for module in get_module(complete_file_name):
                yield module
        
    elif file_name.endswith(".py"):
        module_name = os.path.basename(file_name).split(".py")[0]
        if os.path.sep in file_name :
            package = file_name.split(module_name)[0]
            add_to_path(package)
            
        yield __import__(module_name)
        
def count_loc(lines):
    nb_lines  = 0
    docstring = False
    for line in lines:
        line = line.strip()

        if line == "" \
           or line.startswith("#") \
           or docstring and not (line.startswith('"""') or line.startswith("'''"))\
           or (line.startswith("'''") and line.endswith("'''") and len(line) >3)  \
           or (line.startswith('"""') and line.endswith('"""') and len(line) >3) :
            continue
        
        # this is either a starting or ending docstring
        elif line.startswith('"""') or line.startswith("'''"):
            docstring = not docstring
            continue

        else:
            nb_lines += 1

    return nb_lines

def get_classes(module):
    return filter(lambda x:inspect.getmodule(x) == module,map(operator.itemgetter(1),inspect.getmembers(module,inspect.isclass)))

def get_functions(module):
    return get_callables(inspect.isfunction,module)

def get_methods(klass):
    return get_callables(inspect.ismethod,klass)

def get_callables(type_filter,module_or_class):
    L = map(operator.itemgetter(1),inspect.getmembers(module_or_class,type_filter))
    if type_filter == inspect.isfunction:
        return [item for item in L if is_from_module(item,module_or_class.__name__)]
    #return filter(functools.partial(is_from_module,module_name=module_or_class),L)
    return L

def is_from_module(item,module_name):
    return inspect.getmodule(item).__name__ == module_name

def is_dict_keyword(keyword):
    return not(keyword.startswith("#"))

def is_stats_keyword(keyword):
    return not(is_dict_keyword(keyword))

def main():
    modules_names = sys.argv[1:]
    if not modules_names:
        print "usage : %s FILE_OR_DIRECTORY ... " % sys.argv[0]
        exit(0)

    tahar = Inspector(modules_names)
    tahar.process_modules()
    tahar.print_stats()
        
def test_get_module():
    for module in get_module("."):
        print module
    
if __name__ == "__main__":
    main()

