#!/usr/bin/env python3
"""
By defaut, this program backups a file or set of files. This relies on `zipfile`
module to compress the data into a .zip file format. If it an error occured
during backup operation (permission, file not found, etc), program will stop
unless force mode is on (-F).

Program can also do "full" and "incremental" backups. Full backups will be done
by backing up all files on the specified path while incremental backups make
use of an inventory file that is stored inside the user's home directory.

If the inventory file is not present during the first run, it will be created
by the program making every first run as a "full" backup. In suceeding runs,
assuming inventory file is still intact, program will compare the current
timestamp of a file to the one inside the inventory. If timestamps differ, the
file will be backed up. Any discovered files not included in the inventory are
treated as "new" or "modified" files.

Currently, this will not backup any empty directories.
"""

# ISSUES

# TODO
#   - fix `Duplicate name` warning when next program call is executed too fast
#     that the zip_file was not able to change its filename
#   - add remote backup feature
#   - add bitlocker backup feature
#   - test on android phone
#   - backup empty directories
#   - add logging
#   - add backup size at the end

__author__ = "Merrell Vineza"
__date__ = "09/10/2017 12:54 PHT"
__version__ = "1.0.0"
__email__ = "rell.vineza14@yahoo.com"

import argparse
import os
import zipfile
from shutil import copy
from re import search
from time import time, strftime
from sys import argv
from socket import gethostname


class Backup:
    """
    Base class for backup related tasks
    """

    def initialize_catalog(self):
        """
        Setups backup catalog for incremental or full backup
        """
        self.catalog_old = {}
        self.catalog_new = {}
        try:
            copy(catalog, restore_point)
            with open(catalog) as f:
                for entry in f.readlines():
                    try:
                        debug_msg('initialize_catalog() entry: {}'
                                  .format(entry))
                        entry_name = (entry.strip()).split(separator)[0]
                        entry_timestamp = (entry.strip()).split(separator)[1]
                        self.catalog_old[entry_name] = entry_timestamp
                    except IndexError:
                        error_msg('Catalog broken at entry {}'.format(entry))
                        print('Remove the entry from catalog and restore')
                        print('point then retry the backup')
                        rollback()
                        exit(1)
                f = open(catalog, 'a')
                f.close()
        except FileNotFoundError:
            f = open(catalog, 'w')
            f.close()
            copy(catalog, restore_point)

    def record_backup(self, data):
        """
        Records catalog entries to a dictionary
        """

        if backup_type == 'full' or backup_type == 'inc':
            entry_timestamp = str(os.stat(data).st_mtime)
            self.catalog_new[data] = entry_timestamp

    def create_catalog(self):
        """
        Converts the dictionary entries into a catalog file
        """

        verbose_msg('Creating catalog ..')
        with open(catalog, 'a') as f:
            for i, j in self.catalog_new.items():
                f.write('{}{}{}\n'.format(i, separator, j))

    def exclude_data(self, data):
        """
        Checks if data is included in exclude list
        """

        exclude = 'no'
        if backup_type == 'full' or backup_type == 'inc':
            excludes = self.generate_list(exclude_list)
            for ex in excludes:
                try:
                    pattern = ex.replace('\\', '/')
                    string = data.replace('\\', '/')
                    if search(pattern, string):
                        debug_msg("pattern: {} string: {}".format(pattern,
                                                                  string))
                        verbose_msg('Excluding {} ..'.format(data))
                        exclude = 'yes'
                        return exclude
                # `ValueErorr` is just a placeholder for now
                except ValueError:
                    print("""
                          exclude_data() exception occured when
                          analyzing {}""".format(data))
            return exclude
        else:
            return exclude

    def generate_list(self, file):
        """
        Generates a list from a file
        """

        try:
            with open(file) as f:
                return [line.strip() for line in f.readlines()]
        except FileNotFoundError:
            f = open(file, 'w')
            f.close()
        return []

    def add_file(self, file):
        """
        Adds a file into zip archive
        """

        try:
            exclude = self.exclude_data(file)
            if exclude == 'no':
                debug_msg('exclude {}? {}'.format(file, exclude))
                print('Adding {} ..'.format(file))
                self.backup.write(file)
                self.record_backup(file)
        except (PermissionError, OSError):
            if force:
                verbose_msg('Unable to backup {}. Skipping ..'
                            .format(file))
                pass
            else:
                error_msg('Unable to backup {}'.format(file))
                rollback()
                exit(1)

    def add_directory(self, directory):
        """
        Adds a directory into a zip archive recursively
        """

        for base, sub, files in os.walk(directory):
            for f in files:
                full_path = os.path.join(base, f)
                try:
                    exclude = self.exclude_data(full_path)
                    debug_msg('exclude {}? {}'.format(full_path, exclude))
                    if exclude == 'no':
                        print('Adding {} ..'.format(full_path))
                        self.backup.write(full_path)
                        self.record_backup(full_path)
                except (FileNotFoundError, PermissionError, OSError):
                    if force:
                        verbose_msg('Unable to backup {}. Skipping ..'
                                    .format(full_path))
                        continue
                    else:
                        error_msg('Unable to backup {}'.format(full_path))
                        rollback()
                        exit(1)

    def force_generate_catalog(self, backup_list):
        """
        Method for recording all data needs to be backed up to catalog_new
        dict without actually creating the zip file. For now, this is used
        by IncrementalBackup class.
        """

        for entry in backup_list:
            if os.path.isdir(entry):
                for base, sub, files in os.walk(entry):
                    exclude = self.exclude_data(base)
                    if exclude == 'no':
                        for f in files:
                            try:
                                full_path = os.path.join(base, f)
                                self.record_backup(full_path)
                            except FileNotFoundError:
                                continue
                            except OSError:
                                continue
            if os.path.isfile(entry):
                exclude = self.exclude_data(entry)
                if exclude == 'no':
                    self.record_backup(entry)

    def show_backup_file(self):
        """
        Displays path to zip file created
        """

        try:
            os.stat(zip_file)
            print('Backup created: {}'.format(zip_file))
        except FileNotFoundError:
            print('No backup generated')


class SimpleBackup(Backup):
    """
    Backup method for anything not requiring a catalog. Useful if you want to
    backup a file/directory quickly without any additional setup.
    """

    def __init__(self, data):
        self.data = data

    def run_simple_backup(self):
        """
        Detects if input data is a file or directory and runs the approriate
        method
        """

        with zipfile.ZipFile(zip_file, 'a', compression) as self.backup:
            if os.path.isfile(self.data):
                self.add_file(self.data)
            elif os.path.isdir(self.data):
                self.add_directory(self.data)
            else:
                print('Unkown file type')
                exit(1)
            self.show_backup_file()


class FullBackup(Backup):
    """
    Performs full backup
    """

    def run_full_backup(self):
        """
        Runs the actual full backup
        """

        self.initialize_catalog()

        backup_list_full = self.generate_list(file_list)
        with zipfile.ZipFile(zip_file, 'a', compression) as self.backup:
            for data_full in backup_list_full:
                df = data_full.strip()
                if os.path.isfile(df):
                    self.add_file(df)
                elif os.path.isdir(df):
                    self.add_directory(df)
                else:
                    print('Unkown file type')
                    exit(1)

        self.create_catalog()
        self.show_backup_file()


class IncrementalBackup(Backup):
    """
    Performs an incremental backup by having additional methods that checks
    catalog for timestamp changes
    """

    def check_time_stamp(self, data):
        """
        Determines whether a file's timestamp is new or not
        """

        if data in self.catalog_old and data in self.catalog_new:
            if self.catalog_new[data] > self.catalog_old[data]:
                return 'new'
            else:
                try:
                    assert self.catalog_old[data] == self.catalog_new[data]
                    return 'old'
                except AssertionError:
                    error_msg("assertion error encountered at {}: ".
                              format(data))
                    rollback()
                    exit(1)
        else:
            return 'new'

    def run_inc_backup(self):
        """
        Runs the actual incremental backup
        """

        self.initialize_catalog()
        backup_list_inc = self.generate_list(file_list)
        self.force_generate_catalog(backup_list_inc)

        with zipfile.ZipFile(zip_file, 'a', compression) as self.backup:
            for data_inc in self.catalog_new.keys():
                ts = self.check_time_stamp(data_inc)
                if ts == 'new':
                    if os.path.isfile(data_inc):
                        self.add_file(data_inc)
                    elif os.path.isdir(data_inc):
                        self.add_directory(data_inc)
                    else:
                        verbose_msg('Unkown filetype: {} . skipping ..')
                        continue

        self.create_catalog()
        self.show_backup_file()


def pre_checks():
    """
    Performs initial checks
    """

    if len(argv) == 1:
        print('Use "-h" to see options')
        exit(1)

    try:
        os.mkdir(location)
    except FileExistsError:
        pass

    if backup_type == 'simple':
        try:
            os.stat(target_data)
        except FileNotFoundError:
            print('File not found: {}'.format(target_data))
            exit(1)

    if backup_type == 'inc' or backup_type == 'full':
        try:
            os.stat(file_list)
            verbose_msg('Found backup selection: {}'.format(file_list))
        except FileNotFoundError:
            verbose_msg('Backup selection not found')
            verbose_msg('Creating {} ..'.format(file_list))
            f = open(file_list, 'w')
            f.close()

        if os.stat(file_list).st_size == 0:
            print('Nothing to backup. Add paths on your backup selection.')
            print(file_list)
            exit(1)


def verbose_msg(*message):
    """
    Prints out verbose messages
    """

    if verbose:
        print('Verbose: {}'.format(''.join(message)))


def debug_msg(*message):
    """
    Prints out verbose messages
    """

    if debug:
        print('Debug: {}'.format(''.join(message)))


def error_msg(*message):
    """
    Prints out verbose messages
    """

    print('Error: {}'.format(''.join(message)))


def show_elapsed_time():
    """
    Calculates elapsed time up to hours
    """

    time_end = time()
    time_diff = time_end - time_start
    s = 0
    m = 0
    h = 0
    if time_diff > 60:
        p = time_diff / 60.00
        if p > 60:
            h = str(p / 60.00).split('.')[0]
            m = str(float('.' + str(p / 60.00).
                          split('.')[1]) * 60).split('.')[0]
            s = str(int(str(float('.' + str(p / 60.00)
                                  .split('.')[1]) * 60)
                        .split('.')[1]) * 60)[0:2]
            print("Elapsed time: {} hours, {} minutes, {} seconds".
                  format(h, m, s))
        else:
            m = str(p).split('.')[0]
            s = str(float('.' + str(p).split('.')[1]) * 60).split('.')[0]
            print("Elapsed time: {} minutes, {} seconds".format(m, s))
    else:
        s = float(time_diff)
        print("Elapsed time: {0:.2f} seconds".format(s))


def rollback():
    """
    Uses restore point as a rollback option for corrupted catalog
    """

    if backup_type == 'full' or backup_type == 'inc':
        print('Trying to restore catalog from {} ..'.format(restore_point))
        if os.stat(restore_point).st_size != 0:
            print('')
            try:
                os.remove(catalog)
                copy(restore_point, catalog)
                verbose_msg('Catalog restored.')
            except FileNotFoundError:
                verbose_msg('Restore point not found: {}'.
                            format(restore_point))
                error_msg('Unable to restore catalog.')
        else:
            verbose_msg('Restore point has no content: {}'.
                        format(restore_point))
            error_msg('Unable to restore catalog.')
    else:
        print('')


def initialize_objects():
    """
    Parses command line arguments and create required objects
    """

    global home, hostname
    global debug, verbose, force, location, target_data, backup_type
    global file_list, exclude_list, catalog, restore_point, timestamp
    global extension, filename, zip_file, separator, backup_components
    global compression

    home = os.environ['HOME']
    hostname = gethostname()

    parser = argparse.ArgumentParser()
    parser.add_argument("-f",
                        help="file or directory to backup (simple)")
    parser.add_argument("-d", default=os.path.join(home, 'Backups'),
                        help="backup destination (default: HOME/Backups)")
    parser.add_argument("-D", action="store_true", help="turns on debug mode")
    parser.add_argument("-t", default="simple",
                        help="backup type (simple/remote/btl/inc/full)")
    parser.add_argument("-v", action="store_true", help="turns on verbose mode")
    parser.add_argument("-F", action="store_true", help="force mode")
    args = parser.parse_args()

    debug = args.D
    verbose = args.v
    force = args.F
    location = args.d
    target_data = args.f
    backup_type = args.t

    file_list = os.path.join(home, '.backup_files.lst')
    exclude_list = os.path.join(home, '.backup_exclude.lst')
    catalog = os.path.join(home, '.backup_catalog.db')
    restore_point = os.path.join(home, '.backup_restore_point.db')
    timestamp = strftime('%m-%d-%Y-%H-%M-%S')
    extension = 'zip'
    filename = '{}-backup-{}-{}.{}'.format(hostname,
                                           backup_type,
                                           timestamp,
                                           extension)
    zip_file = os.path.join(location, filename)
    separator = '|'
    compression = zipfile.ZIP_DEFLATED


def run_backup():
    """
    Runs backup based on backup_type chosen by user
    """

    initialize_objects()
    pre_checks()

    try:
        if backup_type == 'simple':
            simple = SimpleBackup(target_data)
            simple.run_simple_backup()
        elif backup_type == 'full':
            full = FullBackup()
            full.run_full_backup()
        elif backup_type == 'inc':
            inc = IncrementalBackup()
            inc.run_inc_backup()
        else:
            print('Unknown backup type, use -h for choices')
            exit(1)
    except KeyboardInterrupt:
        rollback()
        exit(1)


time_start = time()
run_backup()
show_elapsed_time()
