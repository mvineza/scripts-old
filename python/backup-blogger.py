#!/usr/bin/env python
"""
Small script to backup blogger and move the backed up file to correct
destination.
"""

__author__ = "Merrell Vineza"
__date__ = "12/29/2017 15:04:45 PHT"
__version__ = "1.0.0"
__email__ = "rell.vineza14@yahoo.com"

# TODO:
#   - create proper method of singing out instead of destroying the instance
#

from selenium import webdriver
from sys import exit, platform
from time import sleep
from datetime import datetime
from os import stat, path, remove, chown, system
from shutil import move
from pytz import timezone
from pyvirtualdisplay import Display
import logging
try:
    from creds.blogger import blogger_password
except ImportError:
    print('Unable to find blogger password from the ff paths:\n')
    print('{}'.format("\n".join(path)))
    exit(1)


blogger_email = 'merrell14@gmail.com'
blogger_login_url = "https://accounts.google.com/signin/v2/identifier?hl=fil&\
passive=true&service=blogger&ltmpl=blogger&continue=\
https%3A%2F%2Fwww.blogger.com%2Fhome&flowName=GlifWebSignIn&\
flowEntry=ServiceLogin"
blogger_timeout = 60
download_loc = '/home/merrell/Downloads'
dump_loc = '/mnt/sda1_bitlocker/Blogger/'
logfile = '/var/log/backup-blogger.log'


def get_filename():
    fmt = '%m-%d-%Y'
    tmz = datetime.now(timezone('US/Pacific')).strftime(fmt)
    return 'blog-' + tmz + '.xml'


def check_download(filepath):
    size = 0
    while stat(filepath).st_size != size:
        print('Downloading ..')
        size = stat(filepath).st_size
        sleep(1)
    return 'finished'


class BackupBlogger:

    def __init__(self, blogger_email, blogger_login_url, blogger_password):
        self.email = blogger_email
        self.url = blogger_login_url
        self.passwd = blogger_password
        if platform == 'linux':
            display = Display(visible=0, size=(1920, 1080))
            display.start()
        self.browser = webdriver.Chrome()
        self.filename = get_filename()
        self.filepath = path.join(download_loc, self.filename)

    def login(self):
        print('Logging in to blogger ..')
        self.browser.implicitly_wait(blogger_timeout)
        self.browser.get(self.url)
        self.browser.find_element_by_id('identifierId').send_keys(self.email)
        self.browser.find_element_by_id('identifierNext').click()
        self.browser.find_element_by_name('password').send_keys(self.passwd)
        self.browser.find_element_by_id('passwordNext').click()

    def export_content(self):
        print('Exporting backup ..')
        self.browser.find_element_by_id('blog-tab-settings').click()
        self.browser.find_element_by_xpath("//span[text()='Other']").click()
        self.browser.find_element_by_xpath(
            "//button[text()='Back up Content']").click()
        self.browser.find_element_by_xpath(
            "//button[text()='Save to your computer']").click()
        # dirty hack on browser closing too early
        sleep(3)

    def quit(self):
        self.browser.quit()

    def copy_backup(self):
        if check_download(self.filepath) == 'finished':
            try:
                print('Moving {} to {}'.format(self.filepath, dump_loc))
                try:
                    remove(path.join(dump_loc, self.filename))
                except (OSError, FileNotFoundError):
                    pass
                move(self.filepath, dump_loc)
            except (OSError, FileNotFoundError) as msg:
                print('exception occured: {}'.format(msg))
                logging.error(msg)
                exit(1)


def main():
    if not path.isfile(logfile):
        # We are running this script as `merrell` so use `sudo`
        system('sudo touch {}'.format(logfile))
        system('sudo chown merrell {}'.format(logfile))
    logging.basicConfig(filename=logfile,
                        format='%(asctime)s : %(levelname)s : %(message)s',
                        datefmt='%m-%d-%Y %H:%M:%S',
                        level=logging.INFO)
    backup = BackupBlogger(blogger_email, blogger_login_url, blogger_password)
    backup.login()
    backup.export_content()
    backup.quit()
    backup.copy_backup()


if __name__ == '__main__':
    try:
        main()
        logging.info('Backup success')
    except:
        msg = 'Backup failed'
        print(msg)
        logging.error(msg)
        exit(1)
