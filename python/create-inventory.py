#!/usr/bin/env python2
"""
Creates ansible static inventory which gets group information from a yaml file
similar to this:

    vmlist:
    - vm_name: vm01
      group: master
    - vm_name: vm02
      group: node

This only works for now on direct group memberships like:
    [master]
    vm01

    [node]
    vm02
    vm03

This doesn't support groupings with children like:
    [webserver:children]
    nginx
    apache

    [nginx]
    vm01

    [apache]
    vm02

WARNING: If the destination file exists, its content will be wiped out so be
sure to double check the filename or create a backup first.
"""

__author__ = "Merrell Vineza"
__date__ = "02/21/2018 21:22:25 PHT"
__version__ = "1.0.0"
__email__ = "rell.vineza14@yahoo.com"

import yaml
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-s', required=True, help='source yaml file')
parser.add_argument('-d', required=True, help='destination file')
options = parser.parse_args()

src_file = options.s
dst_file = options.d
with open(src_file) as f:
    data = yaml.load(f)

members = [[host['vm_name'], host['group']] for host in data['vm_list']]
groups = set([x[1] for x in members])

with open(dst_file, 'w') as f:
    for g in groups:
        f.write('[{}]\n'.format(g))
        for h in members:
            if h[1] == g:
                f.write('{}\n'.format(h[0]))
        f.write('\n')
