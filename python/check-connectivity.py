#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  check_connectivity.py
#
#  Copyright (c) 2015 IT Ops Linux ESL
#
#  Permission is hereby granted, free of charge, to any person
#  obtaining a copy of this software and associated documentation
#  files (the "Software"), to deal in the Software without
#  restriction, including without limitation the rights to use,
#  copy, modify, merge, publish, distribute, sublicense, and/or
#  sell copies of the Software, and to permit persons to whom the
#  Software is furnished to do so, subject to the following
#  conditions:
#
#  The above copyright notice and this permission notice shall be
#  included in all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
#  KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
#  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#  PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
#  OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
#  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

__author__ = "IT Ops Linux ESL"
__version__ = "$Revision: 1.0 $"
__date__ = "$Date: 2015/04/20 $"
__copyright__ = "Copyright (c) 2015 IT Ops Linux ESL"
__license__ = "Python"

import socket, optparse


def checkconn():
    """
    Checks connectivity to specified host on indicated port and protocol. If protocol is not defined, tcp will be used
    """

    if options.protocol == '' or options.protocol == 'tcp':
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex((options.hostname, options.port))

    if options.protocol == 'udp':
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        result = sock.connect_ex((options.hostname, options.port))

    if result == 0:
        print("Port " + str(options.port) + " is open on " + options.hostname)
    else:
        print("Port " + str(options.port) + " is not open on " + options.hostname)

if __name__ == "__main__":
    parser = optparse.OptionParser(usage='Usage: python %prog -h <hostname> -p <port>')
    parser.add_option('-H', '--hostname', help='Provide hostname to query', default="127.0.0.1")
    parser.add_option('-p', '--port', type=int, help='Provide port number to check', default="80")
    parser.add_option('-P', '--protocol', help='Provide protocol to query (tcp/udp)', default='tcp')
    (options, args) = parser.parse_args()

    checkconn()
