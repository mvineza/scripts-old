#!/usr/bin/env python
"""
Merges new kubeconfig files to existing $HOME/.kube/config,
One limitation at the moment is when you add a config whose
cluster already exists in your $HOME/.kube/config, the user
that will be mapped on that cluster will be the user on the
config you will add. It will remove the existing user you
have in $HOME/.kube/config and replace it with a new one.
"""

__author__ = "Merrell Vineza"
__date__ = "01/13/2019 20:15:36 PHT"
__version__ = "1.0.0"
__email__ = "rell.vineza14@yahoo.com"

import yaml
import os
import sys
from argparse import ArgumentParser

MASTER_KUBECONFIG = os.path.join(os.environ['HOME'], '.kube', 'config')
SETTINGS = ['clusters', 'contexts', 'users']


def get_options():
    parser = ArgumentParser()
    parser.add_argument('-f', help='kubeconfig file to add', required=True)
    return parser.parse_args()


def main():
    args = get_options()
    KUBECONFIG = args.f
    a = open(MASTER_KUBECONFIG)
    b = open(KUBECONFIG)
    original_setting = yaml.load(a)
    additional_setting = yaml.load(b)

    for setting in SETTINGS:
        hits = 0
        for s in original_setting[setting]:
            if s['name'] == additional_setting[setting][0]['name']:
                s.update(additional_setting[setting][0])
                hits += 1
        if hits >= 1:
            pass
        else:
            original_setting[setting].append(additional_setting[setting][0])
        hits = 0
    with open(MASTER_KUBECONFIG, 'w') as master:
        yaml.dump(original_setting, master, default_flow_style=False)


main()
