#!/usr/bin/env python
"""
Gundam related stuffs
"""

__author__ = "Merrell Vineza"
__date__ = "06/03/2018 16:30:49 PHT"
__version__ = "1.0.0"
__email__ = "rell.vineza14@yahoo.com"


import argparse
import os
import re
import string
import sys

default_root_folder = '/home/merrell/Documents/Gundam/ads'

parser = argparse.ArgumentParser()
parser.add_argument('-n', '--name', help='Folder name')
parser.add_argument('-d', '--destination', default=default_root_folder,
                    help='Folder location')
parser.add_argument('-f', '--folderlist', help='Folder list')
options = parser.parse_args()
folders = []
root_folder = options.destination


def new_name(old_name):
    lower = old_name.lower()
    space = re.sub(' +', '_', lower)
    dash = re.sub('-+', '_', space)
    dot = re.sub('\.+', '_', dash)
    slash = re.sub('/+', '_', dot)
    final = slash.strip(string.punctuation)
    newname = final
    return newname


if options.name and options.folderlist:
    print('Can\'t use -n and -f at the same time.')
    sys.exit(1)
elif options.name:
    folders.append(options.name)
elif options.folderlist:
    f = open(options.folderlist)
    for entry in f.readlines():
        folders.append(entry.strip())
else:
    print('Specify -n <folder name> or -f <file containing folder list>')
    sys.exit(1)

for folder in folders:
    folder = os.path.join(root_folder, new_name(folder))
    try:
        os.mkdir(folder)
    except FileExistsError:
        print('Folder already exists: {}'.format(folder))
    else:
        print('Folder created: {}'.format(folder))
