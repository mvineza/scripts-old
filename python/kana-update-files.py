#!/usr/bin/env python3
"""
This will update Kana templates and settings
"""

__author__ = "Merrell Vineza"
__date__ = "12/14/2017 10:57:00 PHT"
__version__ = "1.0.0"
__email__ = "rell.vineza14@yahoo.com"

import os
import argparse
import zipfile
import shutil

parser = argparse.ArgumentParser()
parser.add_argument('-a', '--archive', required=True, help="Archive to extract")
parser.add_argument('-g', '--gitrepo',
                    default='/home/merrell/GitRepo/esl-playbooks',
                    help="Local path of Gitlab repo")
parser.add_argument('-d', '--destination', default="/tmp/kana-templates",
                    help="Path to extract the files")
options = parser.parse_args()

archive = options.archive
destination = options.destination
gitrepo = options.gitrepo
green = '\033[1;32;49m'
white = '\033[1;37;49m'
z = zipfile.ZipFile(archive)

try:
    shutil.rmtree(destination)
except FileNotFoundError:
    pass
os.mkdir(destination)
z.extractall(path=destination)
rootdir = os.listdir(destination)
assert len(rootdir) == 1
rootdir = os.path.join(destination, rootdir[0])
files = [r + '/' + a for r, d, f in os.walk(rootdir) for a in f]
for i in files:
    src = i
    dst = os.path.join(gitrepo, i.replace(destination + '/', ''))
    print(green + '[EXTRACTED] ' + white + dst)
    shutil.copy(src, dst)
